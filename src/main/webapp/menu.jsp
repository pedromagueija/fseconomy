<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
	    import = "net.fseconomy.beans.*, net.fseconomy.data.*, java.util.*"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%!
String groupMenu(UserBean user, String parent, int id, String name, boolean staffOnly, boolean includeBaseLink, String link, String arg, HttpServletResponse response)
{
	Map memberships = user.getMemberships();

	StringBuilder returnValue = new StringBuilder();
	String menu = parent + "_sub_" + id;

	returnValue.append("<li><a href='").append(includeBaseLink ? response.encodeURL(link) : "").append("'>").append(name).append("</a>");
	int count = 0;
	int stringLen = 0;
	boolean hasGroups = false;
    if (memberships != null)
    {
        for (Object o : memberships.values())
        {
            Groups.groupMemberData memberData = (Groups.groupMemberData) o;
            if ((!staffOnly && memberData.memberLevel >= UserBean.GROUP_MEMBER) || memberData.memberLevel >= UserBean.GROUP_STAFF)
            {
                int len = memberData.groupName.length();
                if (len > stringLen)
                {
                    stringLen = len;
                }
                hasGroups = true;
            }
        }
    }
    if (!hasGroups)
    {
		returnValue.append("</li>");
        return includeBaseLink ? returnValue.toString() : "";
    }

	int length = 6 * stringLen + 20;
	returnValue.append("<ul>");
    for (Object o : memberships.values())
    {
        Groups.groupMemberData memberData = (Groups.groupMemberData) o;
        if ((!staffOnly && memberData.memberLevel >= UserBean.GROUP_MEMBER) || memberData.memberLevel >= UserBean.GROUP_STAFF)
        {
			returnValue.append("<li><a href='").append(response.encodeURL(link + arg + memberData.groupId)).append("'>").append(memberData.groupName.replaceAll("\'", "\\\\'")).append("</a></li>"); 
        }
    }
	returnValue.append("</ul></li>");

	return returnValue.toString();
}
%>
<script>
	$( function() 
	{
		$('#nav li:has(ul)').doubleTapToGo();
	});
</script>
<ul class="main-menu" id="nav" role="navigation">
    <li>
        <a href="/index.jsp">Home</a>
        <ul>
        <%
        	if (user.isLoggedIn())
        	{
        %>
            <li><a href="/edituser.jsp">Change preferences</a></li>
            <li><a href="/changepassword.jsp">Change password</a></li>
            <li><a href="/datafeeds.jsp">Data feeds</a></li>
            <li><a href="/score.jsp?type=pilots">Pilots</a></li>
            <li><a href="/score.jsp?type=groups">Groups</a></li>
            <li><a href="/aircraftmodels.jsp">Aircraft models</a></li>
        <%
            }
        %>
            <li><a href="http://www.fseconomy.net/forum" target="_blank">FSE forums</a></li>
        </ul>
    </li>

<%
	if (user.isLoggedIn()) 
	{
%>
        <li><a href="/airport.jsp">Airports</a></li>
        <li><a href="/myflight.jsp">My flight</a></li>
        <li><a href="/log.jsp">Log</a>
            <ul>
                <%= groupMenu(user, "m4", 1, "Logs", false, true, "/log.jsp", "?groupid=", response) %>
				<%= groupMenu(user, "m4", 1, "Payment logs", false, true, "/paymentlog.jsp", "?groupid=", response) %>
            </ul>
        </li>
        <li>
			<a href="/aircraft.jsp">Aircraft</a>
			<ul>
				<%= groupMenu(user, "m6", 1, "Aircraft", false, true, "/aircraft.jsp", "?id=", response) %>
				<li><a href="/aircraftforsale.jsp">Purchase aircraft</a></li>
			</ul>
		</li>
        <li><a href="/banksummary.jsp">Banking</a></li>
        <li><a href="/groups.jsp">Groups</a>
			<ul>
				<li><a href="/groups.jsp">My groups</a></li>
				<li><a href="/grouplist.jsp">All groups</a></li>
				<%= groupMenu(user, "m8", 1, "Assignments", false, false, "/groupassignments.jsp", "?groupid=", response) %>
				<%= groupMenu(user, "m8", 2, "Pay group", false, false, "/pay.jsp", "?groupid=", response) %>
				<%= groupMenu(user, "m8", 3, "Memberships", true, false, "/memberships.jsp", "?groupid=", response) %>
			</ul>
		</li>
		<li><a href="/goods.jsp">Goods</a>
			<ul>
				<%= groupMenu(user, "m9", 1, "Goods", true, true, "/goods.jsp", "?groupid=", response) %>
				<%= groupMenu(user, "m9", 2, "Transfer assignments", true, true, "/goodsassignments.jsp", "?transferid=", response) %>
			</ul>
		</li>
		<li><a href="/fbo.jsp">FBO</a>
			<ul>
				<%= groupMenu(user, "m10", 1, "FBO", true, true, "/fbo.jsp", "?id=", response) %>
				<%= groupMenu(user, "m10", 2, "FBO Mgt", true, true, "/fbomgt.jsp", "?id=", response) %>
				<%= groupMenu(user, "m10", 3, "Facilities", true, true, "/fbofacility.jsp", "?id=", response) %>
				<li><a href="/fbomap.jsp">FBO maps</a></li>
				<li><a href="/fbosforsale.jsp">Purchase FBO</a></li>
				<li><a href="/fbolotteries.jsp">FBO lotteries</a></li>
			</ul>
		</li>
		<!-- <li><a href="menumap.jsp">Sitemap</a></li> -->
<%
	}
	if (user.getLevel() == UserBean.LEV_MODERATOR || user.getLevel() == UserBean.LEV_ADMIN)
	{
%>
		<li class="menu-admin"><a href="/admin/admin.jsp">Admin</a></li>
<%
	}
	if (user.getLevel() == UserBean.LEV_CSR)
	{
%>
		<li class="menu-admin"><a href="/admin/usermanager.jsp">CRS</a></li>
<%
	}
	if (user.getLevel() == UserBean.LEV_ACA)
	{
%>
		<li class="menu-admin"><a href="/admin/aircraftmappings.jsp">Aircraft mapping</a></li>
<% 
	}
%>
</ul>
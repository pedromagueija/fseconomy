<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.util.Helpers"
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
    String message = Helpers.getSessionMessage(request);
    String returnUrl = Helpers.getSessionReturnUrl(request);
%>
<jsp:include flush="true" page="/head.jsp" />
</head>
<body>

<jsp:include flush="true" page="top.jsp" />
<jsp:include flush="true" page="menu.jsp" />

<div id="wrapper">
    <div class="content error">
    <%= message %><br/>
    <a href="<%= returnUrl %>">Back</a>
    </div>
</div>
</body>
</html>

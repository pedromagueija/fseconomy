5/4/2018
textboxio is a product of Ephox and can be found here:
https://textbox.io/

All items under the textboxio folder are considered owned by Ephox and you must comply with any current or future licensing requirements to use their product.

This code is used under under the following conditions or restrictions for FSEconomy:
 textboxio Community Edition
 Free
 for non-commercial usage
 Non-commercial usage only
 No sublicensing or redistribution
 No server-side components
 No support
 Creative Commons CC BY-NC-ND 4.0 license
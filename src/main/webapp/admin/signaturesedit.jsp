<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import="net.fseconomy.beans.*, net.fseconomy.data.* "
%>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session" />

<%
	if (!Accounts.needLevel(user, UserBean.LEV_MODERATOR))
	{
%>
        <script type="text/javascript">document.location.href="index.jsp"</script>
<%
		return; 
	}
%>
<jsp:include flush="true" page="/head.jsp" />
	<script>

		function updateSig(month)
		{
			window.open(
					'/admin/updatesignature.jsp?month=' + month,
					'UpdateSignature',
					'status=no,toolbar=no,height=450,width=600')
		}
	</script>
</head>
<body>

<jsp:include flush="true" page="/top.jsp" />
<jsp:include flush="true" page="/menu.jsp" />

<div class="wrapper">
<div class="content">
		<ol>
<%
	for(int i=1; i<=12; i++)
	{
%>
			<li>
				<a title="Upload new signature background" onclick="updateSig(<%=i%>)">			
					<img src="/static/sig-templates/template<%=i%>.jpg" />
				</a>
			</li>
<%
	}
%>
		</ol>
</div>
</div>
</body>
</html>

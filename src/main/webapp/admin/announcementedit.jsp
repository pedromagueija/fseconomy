<%@page language="java"
        contentType="text/html; charset=ISO-8859-1"
        import=" java.util.List, net.fseconomy.beans.*,  net.fseconomy.data.*"
%>
<%@ page import="net.fseconomy.util.Helpers" %>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="java.util.GregorianCalendar" %>

<jsp:useBean id="user" class="net.fseconomy.beans.UserBean" scope="session"/>

<%
    if (!Accounts.needLevel(user, UserBean.LEV_MODERATOR))
    {
%>
<script type="text/javascript">document.location.href = "index.jsp"</script>
<%
        return;
    }

    boolean done = false;

    AnnouncementBean a = null;
    String message = null;
    Timestamp ts = new Timestamp(GregorianCalendar.getInstance().getTime().getTime());

    String action = request.getParameter("action");
    String sId = request.getParameter("id");

    if (sId != null && sId.length() > 0 && action == null)
    {
        int id = Integer.parseInt(sId);
        try
        {
            a = Announcements.getAnnouncement(id);
            action = "update";
        } catch (DataError e)
        {
            message = e.getMessage();
        }
    }
    else if (action != null)
    {
        if (action.contentEquals("update"))
        {
            String title = request.getParameter("title");
            if (!Helpers.isNullOrBlank(title))
            {
                int id = Integer.parseInt(request.getParameter("editid"));
                a = Announcements.getAnnouncement(id);

                if(request.getParameter("active") != null)
                    a.setActive(true);
                else
                    a.setActive(false);

                a.setUpdated(ts);
                a.setUpdatedBy(user.getId());

                a.setTitle(title);
                String body = request.getParameter("body");
                if (body == null)
                    body = "";

                a.setBody(body);

                if(a.getActive() && a.getPosted() == null)
                    a.setPosted(ts);

                try
                {
                    Announcements.updateEntry(a);
                    done = true;
                }
                catch (DataError e)
                {
                    message = e.getMessage();
                }
            } else
            {
                message = "Error, title cannot be blank!";
            }
        }
        else if(action.contentEquals("new"))
                {
                    a = new AnnouncementBean();
                    action = "add";
                }
        else if(action.contentEquals("add"))
        {
            String title = request.getParameter("title");
            if (!Helpers.isNullOrBlank(title))
            {
                a = new AnnouncementBean();

                if(request.getParameter("active") != null)
                {
                    a.setActive(true);
                }
                else
                {
                    a.setActive(false);
                }

                a.setCreated(ts);
                a.setCreatedBy(user.getId());
                a.setTitle(title);
                String body = request.getParameter("body");
                if (body == null)
                    body = "";

                a.setBody(body);
                if(a.getActive() && a.getPosted() == null)
                    a.setPosted(ts);

                try
                {
                    Announcements.createEntry(a);
                    done = true;
                }
                catch (DataError e)
                {
                    message = e.getMessage();
                }
            }
            else
            {
                message = "Error, title cannot be blank!";
            }
        }
        else if(action.contentEquals("delete"))
        {
            try
            {
                a = new AnnouncementBean();

                int id = Integer.parseInt(request.getParameter("id"));
                Announcements.deleteEntry(id, user.getId());
                done = true;
            }
            catch (DataError e)
            {
                message = e.getMessage();
            }
        }
    }
%>
<jsp:include flush="true" page="/head.jsp" />
<script src="../scripts/textboxio/textboxio.js"></script>
</head>
<body>
<%
    if(done)
    {
%>
<script type="text/javascript">document.location.href = "announcements.jsp"</script>
<%
    }

    if (message != null)
    {
%>

<div><%=message%>
</div>

<%
} else
{
%>
<form method="post" action="/admin/announcementedit.jsp?action=<%=action%>">
    <input type="hidden" name="editid" value="<%=a.getId()%>">
    <label> Active</label><br>
    <input type="checkbox" name="active" <%=a.getActive() ? "checked" : ""%>><br><br>
    <label>Title: </label><br>
    <textarea name="title" rows="2" cols="80"><%=a.getTitle() != null ? a.getTitle() : ""%></textarea><br><br>
    <label>Body:</label> <br>
    <textarea name="body" id="mytextarea" style="width: 100%; height: 400px;"><%=a.getBody()%></textarea><br>
    <input type="submit" title="Save">
</form>
<%
    }
%>

<script type="text/javascript">
    var editor = textboxio.replace('#mytextarea');
</script>
</body>
</html>

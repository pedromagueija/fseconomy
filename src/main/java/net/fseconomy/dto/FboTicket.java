package net.fseconomy.dto;

import java.sql.Timestamp;

public class FboTicket
{
    public int id;
    public int lotteryid;
    public Timestamp purchased;
    public int owner;

    public FboTicket(int id, int lotteryid, Timestamp purchased, int owner)
    {
        this.id = id;
        this.lotteryid = lotteryid;
        this.purchased = purchased;
        this.owner = owner;
    }
}

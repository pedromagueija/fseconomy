package net.fseconomy.dto;

public class CloseAirport implements Comparable<CloseAirport>
{
    public String icao;
    public double distance;
    public double bearing;
    public double fromLat;
    public double fromLon;

    public CloseAirport(String icao, double distance, double bearing, double lat, double lon)
    {
        this.icao = icao;
        this.distance = distance;
        this.bearing = bearing;
        this.fromLat = lat;
        this.fromLon = lon;
    }

    public int compareTo(CloseAirport cairport)
    {
        return cairport.distance == distance ? 0 : cairport.distance < distance ? 1 : -1;
    }
}

package net.fseconomy.beans;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

public class AnnouncementBean implements Serializable
{
    private static final long serialVersionUID = 1L;

    int id;
    Timestamp created;
    int createdBy;
    Timestamp updated;
    int updatedBy;
    Timestamp posted;
    String title;
    String body;
    boolean active;
    boolean deleted;

    /**
     * Constructor for FSMappingBean.
     */
    public AnnouncementBean()
    {
        super();
    }

    public AnnouncementBean(ResultSet rs) throws SQLException
    {
        setId(rs.getInt("id"));
        setCreated(rs.getTimestamp("created"));
        setCreatedBy(rs.getInt("createdby"));
        setUpdated(rs.getTimestamp("updated"));
        setUpdatedBy(rs.getInt("updatedby"));
        setPosted(rs.getTimestamp("posted"));
        setTitle(rs.getString("title"));
        setBody(rs.getString("body"));
        setActive(rs.getBoolean("active"));
        setDeleted(rs.getBoolean("deleted"));
    }

    public void writeBean(ResultSet rs) throws SQLException
    {
        rs.updateTimestamp("created", created);
        rs.updateInt("createdby", createdBy);
        rs.updateTimestamp("updated", updated);
        rs.updateInt("updatedby", updatedBy);
        rs.updateTimestamp("posted", posted);
        rs.updateString("title", title);
        rs.updateString("body", body);
        rs.updateBoolean("active", active);
        rs.updateBoolean("deleted", deleted);
    }

    /**
     * Sets the id.
     * @param id The id to set
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * Returns the id.
     * @return int
     */
    public int getId()
    {
        return id;
    }

    /**
     * Sets the created timestamp.
     * @param created The timestamp to set
     */
    public void setCreated(Timestamp created)
    {
        this.created = created;
    }

    /**
     * Returns the id.
     * @return int
     */
    public Timestamp getCreated()
    {
        return created;
    }

     /** Sets the created timestamp.
      * @param createdby The id to set
     */
    public void setCreatedBy(int createdby)
    {
        this.createdBy = createdby;
    }

    /**
     * Returns the id.
     * @return int
     */
    public int getCreatedBy()
    {
        return createdBy;
    }

    /**
     * Sets the updated timestamp.
     * @param updated The timestamp to set
     */
    public void setUpdated(Timestamp updated)
    {
        this.updated = updated;
    }

    /**
     * Returns the id.
     * @return int
     */
    public Timestamp getUpdated()
    {
        return updated;
    }

    /** Sets the created timestamp.
     * @param updatedby The id to set
     */
    public void setUpdatedBy(int updatedby)
    {
        this.updatedBy = updatedby;
    }

    /**
     * Returns the id.
     * @return int
     */
    public int getUpdatedBy()
    {
        return updatedBy;
    }

    /**
     * Sets the updated timestamp.
     * @param posted The timestamp to set
     */
    public void setPosted(Timestamp posted)
    {
        this.posted = posted;
    }

    /**
     * Returns the id.
     * @return int
     */
    public Timestamp getPosted()
    {
        return posted;
    }

    /**
     * Sets the title text.
     * @param title The text to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * Returns the text.
     * @return text
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Sets the body text.
     * @param body The text to set
     */
    public void setBody(String body)
    {
        this.body = body;
    }

    /**
     * Returns the text.
     * @return text
     */
    public String getBody()
    {
        return body;
    }

    /**
     * Sets the active flag.
     * @param active The value to set
     */
    public void setActive(boolean active)
    {
        this.active = active;
    }

    /**
     * Returns the text.
     * @return text
     */
    public boolean getActive()
    {
        return active;
    }

    /**
     * Sets the deleted flag.
     * @param deleted The value to set
     */
    public void setDeleted(boolean deleted)
    {
        this.deleted = deleted;
    }

    /**
     * Returns the text.
     * @return text
     */
    public boolean getDeleted()
    {
        return deleted;
    }
}